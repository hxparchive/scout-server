// Scout RTM Logger /////////////////////

var path = require('path'),
    fs = require('fs'),
    mq = require('amqplib/callback_api'),
    ini = require('ini')

var config = ini.parse(fs.readFileSync('scoutd.conf', 'utf-8'))

var to_log = function (line, callback) {
    fs.appendFile(config.server.logfile, line.replace(/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, ''), encoding='utf8', function (err) {
        if (err) {
            return callback(new Error('Could not write to log file'))
        } else {
            return callback(null)
        }
    })
}

mq.connect('amqp://localhost', function (err, conn) {
    if (err) {
        console.log(err)
        throw new Exception('Unable to connect to the messaging queue')
    } else {
        conn.createChannel(function (err, ch) {
            var q = 'scout_logger'
            ch.assertQueue(q, {durable: false})
            to_log('LOG: Created logging queue and waiting for messages...\n', function(){})
            ch.consume(q, function(msg) {
                to_log(msg.content.toString(), function (err) {
                    if (err){
                        console.log('Error calling to_log() in logger.js')
                    } else {
                        ch.ack(msg)
                    }
                })
            }, {noAck: false})
        })
    }
})
