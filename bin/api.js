//// SCOUT API ///////////////////////////////////////////

////////////////////////////////////////////////////////// DEPS
var application_root = __dirname,
    express = require('express'),
    serveStatic = require('serve-static')
    exec = require('child_process').exec,
    methodOverride = require('method-override'),
    errorhandler = require('errorhandler'),
    bodyParser = require('body-parser')
    path = require('path'),
    sys = require('sys'),
    ini = require('ini'),
    fs = require('fs'),
    mongo = require('mongojs'),
    Scout = require(__dirname + '/api_modules'), // SCOUT package 'scout.js'
    mq = require('amqplib/callback_api'),
    log_push = require(__dirname + '/log_push').log_push

////////////////////////////////////////////////////////// CLASS INSTANTIATION
var app = express();
var scout = new Scout()

////////////////////////////////////////////////////////// CONFIG
var config = ini.parse(fs.readFileSync('scoutd.conf', 'utf-8'))

////////////////////////////////////////////////////////// DB

// DB SCHEMA
/*
    hostdata - checkin data from the agents
    hostinfo - generic top-level info for each host (hostname, uuid, checkin source, etc.)
    hostsummary - summary data for each host, used for quick status
    config - shared server config
*/

////////////////////////////////////////////////////////// ALLOW XSS / CORS

var allowCrossDomain = function(req, res, next) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
      res.header('Access-Control-Allow-Methods', '*');
      res.header('Access-Control-Allow-Headers', '*');
      res.header('Access-Control-Allow-Headers', 'X-Requested-With, Accept, Origin, Referer, User-Agent, Content-Type, Authorization');

      // intercept OPTIONS method
      if (req.method === 'OPTIONS') {
        res.sendStatus(200);
      }
      else {
        next();
      }
    };

    app.use(allowCrossDomain);   // make sure this is is called before the router
    //app.use(bodyParser());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
      extended: true
    }));
    app.use(methodOverride());
    app.use(errorhandler());
    app.use(express.static(path.join(application_root, "public")));


////////////////////////////////////////////////////////// INTERNAL FUNCTIONS


////////////////////////////////////////////////////////// DEBUG API
if (config.debug.enable_debug_api == '1') {

    app.get('/debug/healthcheck', function (req, res) {
        res.send('SERVER IS  UP.');
    });

    app.get('/debug/showdata', function (req, res) {
        var hostdb = mongo(config.database.url + '/hostdata', [req.query.uuid])
        hostdb.collection(req.query.uuid).find({}, function (err, data) {
            if (err) {
                res.sendStatus(err)
            } else {
                res.set({'Content-Type' : 'application/json'})
                res.send(JSON.stringify(data))
            }
        })
    })

    app.get('/debug/showinfo', function (req, res) {
        scout.getHostInfo(req.query.uuid, config, function(err, data) {
            if (err) {
                res.send(err)
            } else {
                res.set({'Content-Type' : 'application/json'})
                res.send(JSON.stringify(data))
            }
        })
    })

    app.get('/debug/showsummary', function (req, res) {
        scout.getHostSummary(req.query.uuid, config, function(err, data) {
            if (err) {
                res.send(err)
            } else {
                res.set({'Content-Type' : 'application/json'})
                res.send(JSON.stringify(data))
            }
        })
    })

    app.get('/debug/showconfig', function (req, res) {
        scout.getConfig(config.server.uuid, config, function(err, data) {
            if (err) {
                res.send(err)
            } else {
                res.set({'Content-Type' : 'application/json'})
                ret = JSON.stringify(config) + '\n\n' + JSON.stringify(data) + '\n'
                res.send(ret)
            }
        })
    })
}

////////////////////////////////////////////////////////// PUBLIC API
app.post('/api/checkin', function (req, res) {
    scout.checkAuthKey(config.server.auth_key, req.headers.auth_key, function (err) {
        if (err) {
            res.sendStatus(403)
        } else {
            scout.checkInData(req, config, function(err) {
                if (err) {
                    console.log(err)
                    scout.logger(config.server.logfile, 'There was a problem checking in', '200', err, function(err) {
                        if (err) {
                            console.log('There was an error writing to the log ' + err)
                            res.sendStatus(503)
                        } else {
                            res.sendStatus(503)
                        }
                    })
                } else {
                    scout.logger(config.server.logfile, 'Agent ' + req.body.config.hostname + ' (' + req.headers.uuid + ') checked in.', '100',err, function (err) {
                        if (err) {
                            console.log('There was an error writing to the log ' + err)
                        }
                    })
                    res.sendStatus(200)
                }
            })
        }
    })
})

app.post('/api/checkuuid', function (req, res) {
    scout.checkAuthKey(config.server.auth_key, req.headers.auth_key, function (err) {
        if (err) {
            res.sendStatus(403)
        } else {
            scout.checkUuid(req.query.uuid, config, function (err, data) {
                if (err) {
                    console.log(err)
                    scout.logger(config.server.logfile, 'There was a problem checking the UUID ' + req.query.uuid, '200', err, function (err) {
                        if (err) {
                            console.log(err)
                            res.sendStatus(503)
                        }
                    })
                    res.sendStatus(503)
                } else {
                    if (data == 200) {
                        console.log('UUID ' + req.query.uuid + ' was created')
                        res.sendStatus(200)
                    } else {
                        console.log('UUID ' + req.query.uuid + ' exists!')
                        res.sendStatus(302)
                    }
                }
            })
        }
    })
})

app.post('/api/create_db', function (req, res) {
    scout.checkAuthKey(config.server.auth_key, req.headers.auth_key, function (err) {
        if (err) {
            res.sendStatus(403)
        } else {
            scout.createDb(req.query.uuid, config, function (err) {
                if (err) {
                    console.log(err)
                    scout.logger(config.server.logfile, 'Could not create TTL index for collection ' + req.query.uuid, err, function (err) {
                        if (err) {
                            console.log(err)
                            res.sendStatus(503)
                        }
                    })
                } else {
                    console.log('TTL index for ' + req.query.uuid + ' was created')
                    res.sendStatus(200)
                }
            })
        }
    })
})

////////////////////////////////////////////////////////// API STARTUP / LISTEN
app.listen(config.server.api_port);
console.log('API listening on port ' + config.server.api_port);
