var mq = require('amqplib/callback_api'),
    mongo = require('mongojs'),
    fs = require('fs'),
    ini = require('ini'),
    log_push = require('../bin/log_push').log_push

var config = ini.parse(fs.readFileSync('scoutd.conf', 'utf-8'))

var db_push = function (data, callback) {
    uuid = data.properties.headers.uuid
    dbname = data.properties.headers.dbname
    try {
        log_push('DB: ' + obj.timestamp, function (err) { if (err) { console.log(err) }})
        mongo(config.database.url + '/' + dbname, [uuid]).collection(uuid).save(JSON.parse(data.content.toString()), function (err) {
            if (err) {
                log_push('DB: ' + err, function(err) { if (err) { console.log(err) }})
                return callback(new Error(err))
            } else {
                return callback(null)
            }
        })
    } catch (e) {
        log_push('DB: ' + e, function(e) { if (err) { console.log(err) }})
        return callback(new Error(e))
    }

}

mq.connect('amqp://localhost', function (err, conn) {
    if (err) {
        log_push('DB: ' + err)
    } else {
        conn.createChannel(function (err, ch) {
            var q = 'db_push'
            ch.assertQueue(q, {durable: false})
            ch.consume(q, function(msg) {
                db_push(msg, function (err) {
                    if (err) {
                        log_push('DB: ' + err)
                    } else {
                        ch.ack(msg)
                    }
                })
            }, {noAck: false})
        })
    }
})

// announce startup to main proc
console.log('DB worker is running...')
