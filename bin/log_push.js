var mq = require('amqplib/callback_api')

var log_push = function (msg, callback) {
    mq.connect('amqp://localhost', function (err, conn) {
        if (err) {
            console.log(err)
            throw new Exception('Unable to connect to the messaging queue')
        } else {
            conn.createChannel(function(err, ch) {
                var q = 'scout_logger'
                ch.assertQueue(q, {durable: false})
                ch.sendToQueue(q, new Buffer(msg))
            })
        }
    })
    return callback(null)
}

exports.log_push = log_push
