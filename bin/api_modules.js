// scout.js

var fs = require('fs'),
    path = require('path'),
    mongo = require('mongojs')


var scout = function () {
    this.logger = function (logfile, message, code, errtext, callback) {
        var msg = {}
        msg.timestamp = new Date().toString()
        msg.code = code
        msg.message = message
        msg.err = errtext
        fs.appendFileSync(logfile, JSON.stringify(msg) + '\n', encoding='utf8', function (err) {
            if (err) {
                return callback(new Error('could not write to log file'))
            } else {
                return callback(null)
            }
        })
    }

    this.checkAuthKey = function (server_key, auth_key, callback) {
        if (server_key != auth_key) {
            return callback(new Error(401))
        } else {
            return callback(null, 200)
        }
    }

    this.checkInData = function (data, config, callback) {
        data.body.data.timestamp = new Date() // for mongo TTL
        db = mongo(config.database.url + '/hostdata', [data.headers.uuid])
        db.collection(data.headers.uuid).save(data.body.data, function (err) {
            if (err) {
                db.close()
                return callback(new Error(503))
            } else {
                db.close()
                return callback(null, 200)
            }
        })
    }

    this.getHostInfo = function (uuid, config, callback) {
        db = mongo(config.database.url + '/hostinfo', [uuid])
        db.collection(uuid).findOne(function (err, data) {
            if (err) {
                db.close()
                return callback(new Error(503))
            } else {
                db.close()
                return callback(null, data)
            }
        })
    }

    this.getHostData = function (uuid, config, callback) {
        db = mongo(config.database.url + '/hostdata', [uuid])
        db.collection(uuid).find(function (err, data) {
            if (err) {
                db.close()
                return callback(new Error (503))
            } else {
                db.close()
                return callback(null, data)
            }
        })
    }

    this.getConfig = function (uuid, config, callback) {
        db = mongo(config.database.url + '/config', [config.cluster.cluster_id])
        db.collection(config.cluster.cluster_id).find(function (err, data) {
            if (err) {
                db.close()
                return callback(new Error (503))
            } else {
                db.close()
                return callback(null, data)
            }
        })
    }

    this.checkUuid = function (uuid, config, callback) {
        db = mongo(config.database.url + '/hostdata', [uuid])
        db.collection(uuid).findOne(function (err, data) {
            if (err) {
                db.close()
                return callback(new Error (err))
            } else {
                if (data == null) {
                    db.close()
                    return callback(null, 200)
                } else {
                    db.close()
                    return callback(null, 201)
                }
            }
        })
    }

    this.createDb = function (uuid, config, callback) {
        // create TTL index with a 1yr TTL
        db = mongo(config.database.url + '/hostdata', [uuid])
        db.collection(uuid).ensureIndex( { 'timestamp' : 1 }, { expireAfterSeconds: 31556952 }, function (err) {
            if (err) {
                db.close()
                return callback(new Error (err))
            } else {
                db.close()
                return callback (null, 200)
            }
        })
    }
}

module.exports = scout
