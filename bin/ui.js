//// SCOUT UI ///////////////////////////////////////////

var application_root = __dirname,
    childProcess = require('child_process'),
    express = require('express'),
    servestatic = require('serve-static'),
    exec = require('child_process').exec,
    methodOverride = require('method-override'),
    errorhandler = require('errorhandler'),
    //bodyParser = require('body-parser')
    path = require('path'),
    sys = require('sys'),
    ini = require('ini'),
    fs = require('fs'),
    mongo = require('mongojs'),
    Scout = require(__dirname + '/api_modules')

////////////////////////////////////////////////////////// CLASS INSTANTIATION
var app = express();
var scout = new Scout()
var count = null
var __webroot = process.cwd() + '/webroot'

////////////////////////////////////////////////////////// SOCKET.IO SETUP
var http = require('http').Server(app),
    io = require('socket.io')(http)

////////////////////////////////////////////////////////// CONFIG
var config = ini.parse(fs.readFileSync('scoutd.conf', 'utf-8'))


////////////////////////////////////////////////////////// ALLOW XSS / CORS
var allowCrossDomain = function(req, res, next) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
      res.header('Access-Control-Allow-Methods', '*');
      res.header('Access-Control-Allow-Headers', '*');
      res.header('Access-Control-Allow-Headers', 'X-Requested-With, Accept, Origin, Referer, User-Agent, Content-Type, Authorization');

      // intercept OPTIONS method
      if (req.method === 'OPTIONS') {
        res.sendStatus(200);
      }
      else {
        next();
      }
    };

    app.use(allowCrossDomain);   // make sure this is is called before the router
    //app.use(bodyParser());
    app.use(methodOverride());
    app.use(errorhandler());
    app.use(express.static(path.join(application_root, "public")));


////////////////////////////////////////////////////////// INTERNAL FUNCTIONS

var get_data = function (uuid, callback) {
    // get data from the DB here
    callback(data)
}

////////////////////////////////////////////////////////// FOLDER REDIRECTS

app.get('/', function (req, res) {
    res.redirect('/home')
})

app.get('/css/*', function (req, res) {
    res.sendFile(__webroot + req.path)
})

app.get('/img/*', function (req, res) {
    res.sendFile(__webroot + req.path)
})

app.get('/js/*', function (req, res) {
    res.sendFile(__webroot + req.path)
})

////////////////////////////////////////////////////////// PAGES

app.get('/home', function (req, res) {
    res.sendFile(__webroot + '/index.html')
})

////////////////////////////////////////////////////////// API STARTUP / LISTEN
http.listen(config.server.http_port)
console.log('UI server listening on port ' + config.server.http_port)
